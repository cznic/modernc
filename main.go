// Adapted from
//
// github.com/GoogleCloudPlatform/golang-samples/appengine/helloworld.go
//
// which has the following copyright notice:

// Copyright 2018 Google Inc. All rights reserved.
// Use of this source code is governed by the Apache 2.0
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"modernc.org/modernc/html"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Llongfile)

	go html.Poll(time.Hour, "")

	http.HandleFunc("/-/builder/", html.Serve)
	http.HandleFunc("/", Redirect("git", "modernc.org/*", "https://gitlab.com/cznic/*"))

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		log.Printf("Defaulting to port %s", port)
	}

	log.Printf("INFO: 2023-05-17 20:53 Listening on port %s", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
