# modernc

This directory holds the App Engine app for modernc.org, a simple Godoc redirector.

Forked from [https://github.com/rsc/swtch/tree/master/app/rsc-io](https://github.com/rsc/swtch/tree/master/app/rsc-io).
