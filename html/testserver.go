// Copyright 2020 The Builder Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build none
// +build none

package main

import (
	"fmt"
	"net/http"
	"os"
	"runtime"
	"strings"
	"time"

	"modernc.org/modernc/html"
)

func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
	}
	return fmt.Sprintf("%s:%d:%s", fn, fl, fns)
}

func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	return fmt.Sprintf("%s: TODO %s", origin(2), s) //TODOOK
}

func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}

func main() {
	go html.Poll(time.Hour, `
		modernc.org/kv	3900x	2020-11-18T19:39:46+01:00	b80b2a24e600aa64add307cee4b6ea318ef7cfa3	linux	amd64	PASS [go test -timeout 24h]
		modernc.org/kv	e5-1650	2020-11-18T17:12:38+01:00	b80b2a24e600aa64add307cee4b6ea318ef7cfa3	linux	amd64	PASS [go test -timeout 24h]
		modernc.org/kv	hpnb	2020-11-18T21:25:36+01:00	b80b2a24e600aa64add307cee4b6ea318ef7cfa3	linux	386	PASS [go test -timeout 24h]
		modernc.org/libc	e5-1650	2020-11-19T12:33:05+01:00	b3aa1596bd495d3feff88f563519b18c92b399ce	linux	amd64	PASS [go test -timeout 24h]
		modernc.org/mathutil	3900x	2020-11-18T19:41:27+01:00	2a1ec38753120140e021fd5f51912fc600f7f4de	linux	amd64	PASS [go test -timeout 24h]
		modernc.org/mathutil	e5-1650	2020-11-18T17:14:28+01:00	2a1ec38753120140e021fd5f51912fc600f7f4de	linux	amd64	PASS [go test -timeout 24h]
		modernc.org/mathutil	hpnb	2020-11-18T21:33:37+01:00	2a1ec38753120140e021fd5f51912fc600f7f4de	linux	386	PASS [go test -timeout 24h]
		modernc.org/ql	3900x	2020-11-18T19:44:27+01:00	462784f382fc508dc4b5e46d4e7db1b62849cd6f	linux	amd64	PASS [go test -timeout 24h]
		modernc.org/ql	e5-1650	2020-11-18T17:20:33+01:00	462784f382fc508dc4b5e46d4e7db1b62849cd6f	linux	amd64	FAIL [go test -timeout 24h]
		modernc.org/ql	hpnb	2020-11-18T21:37:44+01:00	462784f382fc508dc4b5e46d4e7db1b62849cd6f	linux	386	PASS [go test -timeout 24h]
		modernc.org/sqlite	3900x	2020-11-18T22:33:27+01:00	91fc7a88e5cbcb745d181771500efedfd798f2e3	linux	amd64	PASS [go test -timeout 24h]
		modernc.org/sqlite	e5-1650	2020-11-18T20:56:54+01:00	91fc7a88e5cbcb745d181771500efedfd798f2e3	linux	amd64	PASS [go test -timeout 24h]
		modernc.org/sqlite	hpnb	2020-11-19T02:18:11+01:00	91fc7a88e5cbcb745d181771500efedfd798f2e3	linux	386	PASS [go test -timeout 24h]
	`)

	http.HandleFunc("/-/builder/", html.Serve)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
